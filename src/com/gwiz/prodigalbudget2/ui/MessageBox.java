/**
 * MessageBox.java
 * 
 * All rights and source belongs to Paul Galasti(pgalasti@gmail.com).
 * 
 * This file is associated with 'Prodigal Budget' Application for Android devices.
 */

package com.gwiz.prodigalbudget2.ui;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.data.NotificationRecord;
import com.gwiz.prodigalbudget2.database.ProdigalBudgetDatabase;
import com.gwiz.prodigalbudget2.database.ProdigalDatabaseSingleton;

/**
 * A view model for displaying a message box notification to the user. A {@link NotificationRecord}
 * can be loaded into the view. Embedded views include:
 * 		</br><li>{@link TextView} of the notification text</li>
 * 		</br><li>{@link ImageView} of the dismiss image</li>
 * </br></br>
 * 
 * <b>TODO Current logic resides in view to dismiss notification record from database. 
 * This should be delegated to a different system instead of the UI class.</b> 
 * @author pgalasti@gmail.com
 */
public class MessageBox extends LinearLayout {

	private Context context = null;
	
	private ImageView cancel = null;
	private TextView text = null;
	
	private NotificationRecord notificationRecord = null;
	private boolean isCleared = false;
	
	
	/**
	 * Creates a blank MessageBox view.
	 * Text is set blank.
	 * @param context The application {@link Context}.
	 */
	public MessageBox(Context context) {
		super(context);
		View.inflate(context, R.layout.info_box, null);

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.info_box, this, true);

		cancel = (ImageView)findViewById(R.id.info_cancel_notification);
		text = (TextView) findViewById(R.id.info_box_message);
		this.context = context;
	}
	
	/**
	 * Loads the contents of a {@link NotificationRecord} into the MessageBox view. Current
	 * model will dismiss the {@link NotificationRecord} from the database to not show up again.
	 * 
	 * @param context The application {@link Context}.
	 * @param notificationRecord {@link NotificationRecord} with contents to load into the view.
	 */
	public MessageBox(Context context, NotificationRecord notificationRecord) {
		this(context);
		this.notificationRecord = notificationRecord;
	}

	/**
	 * Default constructor, don't use.
	 * @param context
	 * @param attr
	 */
	public MessageBox(Context context, AttributeSet attr) {
		super(context, attr);
	}
	
	/**
	 * Returns the {@link ImageView} of the cancel image button.
	 * @return The {@link ImageView} of the cancel image button.
	 */
	public ImageView getCancelImage() {
		return cancel;
	}
	
	/**
	 * Sets the text of the MessageBox {@link TextView}
	 * @param text The text of the MessageBox {@link TextView}
	 */
	public void setText(String text) {
		this.text.setText(text);
		
		invalidate();
		requestLayout();
	}
	
	/**
	 * Sets the text of the MessageBox {@link TextView} using the contents of a {@link NotificationRecord}
	 * @param notificationRecord The {@link NotificationRecord} with contents to populate.
	 */
	public void setText(NotificationRecord notificationRecord) {
		this.notificationRecord = notificationRecord;
		this.setText(notificationRecord.notificationMessage);
	}
	
	/**
	 * Starts the intro slide animiation for the view.
	 */
	public void startNotificationAnimation() {
		Animation rotateAnimation = AnimationUtils.loadAnimation(context,  R.animator.add_to_screen);
		this.startAnimation(rotateAnimation);
	}
	
	/**
	 * Starts the slide animiation for the view when dismissed.
	 * Also, current model will remove the {@link NotificationRecord} from the database.
	 */
	public void cancelNotifiaction() {
		Animation clearAnimation = AnimationUtils.loadAnimation(context,  R.animator.move_off_screen);
		this.startAnimation(clearAnimation);
		isCleared = true;
		notificationRecord.bReported = true;
		
		// TODO async this
		ProdigalBudgetDatabase db = ProdigalDatabaseSingleton.getInstance(context);
		notificationRecord.delete(db);
	}
	
	
	@Override
	protected void onAnimationEnd() {
		super.onAnimationEnd();
		if( isCleared ) {
			this.setVisibility(View.GONE);
			isCleared = false;
		}
		
		invalidate();
		requestLayout();
		super.onAnimationEnd(); // TODO Test this.
	} 
}
