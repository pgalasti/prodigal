/**
 * BudgetItem.java
 * 
 * All rights and source belongs to Paul Galasti(pgalasti@gmail.com).
 * 
 * This file is associated with 'Prodigal Budget' Application for Android devices.
 */

package com.gwiz.prodigalbudget2.ui;

import java.text.NumberFormat;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.data.BudgetRecord;

/**
 * A view model for displaying a budget to the user with minimal details including
 * 		</br><li>{@link ProgressBar} of current budget limit spent</li>
 * 		</br><li>{@link TextView} of the budget name</li>
 * 		</br><li>{@link TextView} of the budget name</li>
 * </br>
 * 
 * @author pgalasti@gmail.com
 */
public class BudgetItem extends RelativeLayout { 

	private ProgressBar 	progress = null;
	private TextView 		budgetName = null;
	private TextView 		budgetLimit = null;

	private Context 		context;
	private BudgetRecord 	budgetRecord = null;
	
	
	/**
	 * A blank BudgetItem view.
	 * Text, value, and progress is blank.
	 * @param context The application {@link Context}.
	 */
	public BudgetItem(Context context) {
		super(context);
		
		View.inflate(context, R.layout.budget_item, null);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.budget_item, this, true);

		this.progress = (ProgressBar)findViewById(R.id.bi_budget_progress);
		this.budgetName = (TextView) findViewById(R.id.budget_text);
		this.budgetLimit = (TextView) findViewById(R.id.budget_limit);
		this.context = context;
		this.budgetLimit.setText("");
	}
	
	/**
	 * Creates a BudgetItem view based on {@link BudgetRecord} contents.
	 * @param context The application {@link Context}.
	 * @param budgetRecord {@link BudgetRecord} with contents to load into BudgetItem.
	 */
	public BudgetItem(Context context, BudgetRecord budgetRecord) {
		
		super(context);
		
		View.inflate(context, R.layout.budget_item, null);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.budget_item, this, true);

		this.budgetRecord = budgetRecord;
		this.progress = (ProgressBar)findViewById(R.id.bi_budget_progress);
		this.budgetName = (TextView) findViewById(R.id.budget_text);
		this.budgetLimit = (TextView) findViewById(R.id.budget_limit);
		this.context = context;
		
		final int PROGRESS = (int)((this.budgetRecord.getCurrentToLimitRatio())*100);
		if( PROGRESS < 75 )
			budgetLimit.setTextColor(getResources().getColor(R.color.lime_green));
		else if ( PROGRESS < 100 ) 
			budgetLimit.setTextColor(getResources().getColor(R.color.warning_orange));
		else
			budgetLimit.setTextColor(getResources().getColor(R.color.danger_red));

		NumberFormat format = NumberFormat.getCurrencyInstance();
		String currentCurrencyText = format.format(budgetRecord.current);
		String limitCurrencyText = format.format(budgetRecord.limit);
		budgetLimit.setText(String.format("%s of %s", currentCurrencyText, limitCurrencyText));
		
		setBudgetProgress(PROGRESS);
		setBudgetName(this.budgetRecord.budgetName);
	}

	/**
	 * Generic constructor. Don't use.
	 * @param context
	 * @param attr
	 */
	public BudgetItem(Context context, AttributeSet attr) {
		super(context, attr);
	}

	
	/**
	 * Sets the embedded {@link ProgressBar}. to the appropriate bar graph length.
	 * Values range between [0-100].
	 * @param nProgress [0-100] value the embedded {@link ProgressBar} should fill.
	 */
	public void setBudgetProgress(int nProgress) {
		Drawable progressDrawable;
		if( nProgress < 75 )
			progressDrawable = getResources().getDrawable(R.drawable.budge_usage);
		else if( nProgress < 100 )
			progressDrawable = getResources().getDrawable(R.drawable.budget_usage_orange);
		else
			progressDrawable = getResources().getDrawable(R.drawable.budget_usage_red);

		progress.setProgress(nProgress);
		progress.setProgressDrawable(progressDrawable);
		
		invalidate();
		requestLayout();
	}

	/**
	 * Sets the embedded {@link TextView} for the budget name.
	 * @param name The budget name.
	 */
	public void setBudgetName(String name) {
		budgetName.setText(name);
		
		invalidate();
		requestLayout();
	}
	
	/**
	 * Starts the slide animiation for the view with a defined delay in milliseconds.
	 * @param delay The delay to start the slide animiation.
	 */
	public void startBudgetItemSlideAnimation(int delay) {
		Animation translateAnimation = AnimationUtils.loadAnimation(this.context,  R.animator.add_to_screen_plain);
		translateAnimation.setStartOffset(delay);
		this.startAnimation(translateAnimation);
	}
	
	/**
	 * Returns the {@link BudgetRecord} loaded into the view.
	 * @return The {@link BudgetRecord} loaded into the view.
	 */
	public BudgetRecord getBudgetRecord() {
		return this.budgetRecord;
	}
}
