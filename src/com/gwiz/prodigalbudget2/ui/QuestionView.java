/**
 * MessageBoxManager.java
 * 
 * All rights and source belongs to Paul Galasti(pgalasti@gmail.com).
 * 
 * This file is associated with 'Prodigal Budget' Application for Android devices.
 */

package com.gwiz.prodigalbudget2.ui;

import java.util.ArrayList;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.gwiz.prodigalbudget2.R;

public class QuestionView extends LinearLayout {

	private ArrayList<RadioButton> buttonArray = new ArrayList<RadioButton>();
	private ArrayList<String> questionSelection = new ArrayList<String>();
	
	private RadioGroup radioGroup;
	private TextView questionText;
	
	public QuestionView(Context context)
	{
		super(context);
		
		View.inflate(context, R.layout.question_layout, null);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.question_layout, this, true);
		
		radioGroup = (RadioGroup)findViewById(R.id.question_radio_group);
		questionText = (TextView)findViewById(R.id.question_text);
		
	}
	public QuestionView(Context context, AttributeSet attrs) {
		super(context, attrs);

		View.inflate(context, R.layout.question_layout, null);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.question_layout, this, true);
		
		radioGroup = (RadioGroup)findViewById(R.id.question_radio_group);
		questionText = (TextView)findViewById(R.id.question_text);
		
	}

	public void setQuestionText(String text) {
		questionText.setText(text);
	}
	
	public void addRadioButton(String text)	{
		RadioButton button = new RadioButton(getContext());
		radioGroup.addView(button);
	}

	public void setButtons(RadioGroup group)
	{
		radioGroup = group;
	}
	public String getText()
	{
		return questionText.getText().toString();
	}
	
	public RadioGroup getRadioGroup()	{
		return radioGroup;
	}
	
	public void setSelectionList(ArrayList<String> selection) 	{
		questionSelection = selection;
	}
	
	public ArrayList<String> getSelectionList() {
		return questionSelection;
	}
	
}
