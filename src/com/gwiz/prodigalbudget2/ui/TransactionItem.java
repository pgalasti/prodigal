package com.gwiz.prodigalbudget2.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.data.BudgetRecord;
import com.gwiz.prodigalbudget2.data.TransactionRecord;

public class TransactionItem extends LinearLayout {

	
	private TransactionRecord transactionRecord;
	
	private LinearLayout transactionColor;
	private CheckBox checkbox;
	private TextView commentText;
	private TextView categoryText;
	private TextView amountText;
	
	public TransactionItem(Context context) {
		super(context);
		
		View.inflate(context, R.layout.transaction_item, null);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.transaction_item, this, true);
		
		transactionColor = (LinearLayout)findViewById(R.id.transaction_color_indicator);
		checkbox = (CheckBox)findViewById(R.id.transaction_item_check);
		commentText = (TextView)findViewById(R.id.transaction_item_comment);
		categoryText = (TextView)findViewById(R.id.transaction_item_category);
		amountText = (TextView)findViewById(R.id.transaction_item_amount);
	}

	public TransactionItem(Context context, BudgetRecord budgetRecord)
	{
		super(context);
		
		View.inflate(context, R.layout.transaction_item, null);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.transaction_item, this, true);
		
		transactionColor = (LinearLayout)findViewById(R.id.transaction_color_indicator);
		checkbox = (CheckBox)findViewById(R.id.transaction_item_check);
		commentText = (TextView)findViewById(R.id.transaction_item_comment);
		categoryText = (TextView)findViewById(R.id.transaction_item_category);
		amountText = (TextView)findViewById(R.id.transaction_item_amount);
	}
}
