/**
 * MessageBoxManager.java
 * 
 * All rights and source belongs to Paul Galasti(pgalasti@gmail.com).
 * 
 * This file is associated with 'Prodigal Budget' Application for Android devices.
 */

package com.gwiz.prodigalbudget2.ui;

import java.util.ArrayList;
import java.util.Iterator;

import android.content.Context;
import android.view.ViewGroup;

import com.gwiz.prodigalbudget2.data.NotificationRecord;

/**
 * The MessageBoxManager class manages {@link MessageBox} objects added to the UI.
 * This class should be used to decouple UI controller logic from the rest of the activity.
 * </br>
 * @author pgalasti@gmail.com
 *
 */
public class MessageBoxManager { 

	private ViewGroup parent = null;
	private Context context = null;
	
	private ArrayList<MessageBox> messageBoxList = new ArrayList<MessageBox>();
	
	/**
	 * Constructs a MessageBoxManager with the assigned parent of several {@link MessageBox} 
	 * objects.
	 * @param hostParent The parent to add the {@link MessageBox} objects to. 
	 */
	public MessageBoxManager(ViewGroup hostParent) {
		parent = hostParent;
		context = parent.getContext();
	}
	
	
	/**
	 * Creates a resulting {@link MessageBox} object loaded by a {@link NotificationRecord}.
	 * The newly created MessageBox will be managed, added parent view, and invoke the animation.
	 * @param notificationRecord The {@link NotificationRecord} to load the {@link MessageBox} from.
	 * @return The newly created {@link MessageBox}.
	 */
	public MessageBox addMessage(NotificationRecord notificationRecord) {
		MessageBox box = new MessageBox(context);
		box.setText(notificationRecord);
		box.startNotificationAnimation();
		
		parent.addView(box);
		messageBoxList.add(box);
		return box;
	}
	
	/**
	 * Creates a resulting {@link MessageBox} object loaded by a simple String.
	 * The newly created MessageBox will be managed, added parent view, and invoke the animation.
	 * @param strMessage The text to load the message box with.
	 * @return The newly created {@link MessageBox}.
	 */
	public MessageBox addMessage(String strMessage) {
		MessageBox box = new MessageBox(context);
		box.setText(strMessage);
		box.startNotificationAnimation();
		
		parent.addView(box);
		messageBoxList.add(box);
		return box;
	}
	
	/**
	 * Removes a {@link MessageBox} from being managed.
	 * @param boxToRemove The {@link MessageBox} object being removed.
	 */
	public void removeMessage(MessageBox boxToRemove) {
		messageBoxList.remove(boxToRemove);
	}
	
	/**
	 * Removes all {@link MessageBox} objects from being managed and invokes 
	 * {@link MessageBox.cancelNotification()}.
	 */
	public void clearAll() {
		for(Iterator<MessageBox> iter = messageBoxList.iterator(); iter.hasNext();) 
			iter.next().cancelNotifiaction();
		
		messageBoxList.clear();
	}
	
	/**
	 * Returns the number of {@link MessageBox} objects being managed. 
	 * @return The number of {@link MessageBox} objects being managed. 
	 */
	public final int size() {
		return messageBoxList.size();
	}
}
