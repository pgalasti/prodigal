package com.gwiz.prodigalbudget2.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.gwiz.prodigalbudget2.R;

public class TitleSpinner extends LinearLayout {

	LinearLayout parent;
	TextView title;
	TextView subTitle;
	Context context;
	
	public TitleSpinner(Context context) {
		super(context);
		this.context = context;
		Init();
	}
	
	public TitleSpinner(Context context, AttributeSet attr) {
		super(context, attr);
		this.context = context;
		Init();
	}
	
	private void Init() {
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.action_spinner, this, true);
		
		parent = (LinearLayout)findViewById(R.id.ts_main);
		title = (TextView)findViewById(R.id.ts_title);
		subTitle = (TextView)findViewById(R.id.ts_subtitle);
		title.setText("Prodigal Budget");
		
	}
	
	public void setAdapter(SpinnerAdapter aa) {
	}

}
