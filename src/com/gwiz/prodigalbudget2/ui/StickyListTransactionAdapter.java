package com.gwiz.prodigalbudget2.ui;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gwiz.androidutils.ui.stickylistheaders.StickyListHeadersAdapter;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.data.TransactionRecord;

public class StickyListTransactionAdapter extends BaseAdapter implements StickyListHeadersAdapter {

	private LayoutInflater inflater;	
	private TransactionRecord[] _Transactions;
	
	public StickyListTransactionAdapter(Context context, TransactionRecord[] transactions)
	{
		inflater = LayoutInflater.from(context);
		_Transactions = transactions;
	}
	
	@Override
	public boolean areAllItemsEnabled() {
		return true;
	}

	@Override
	public boolean isEnabled(int position) { 
		return true;
	}

	@Override
	public int getCount() {
		return _Transactions.length;
	}

	@Override
	public Object getItem(int position) {
		return _Transactions[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getItemViewType(int position) {
		return IGNORE_ITEM_VIEW_TYPE;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		View view = inflater.inflate(R.layout.transaction_item, null );
		return view;
	}

	@Override
	public int getViewTypeCount() {
		return getCount();
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isEmpty() {
		return getCount() == 0;
	}

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
		
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		
		convertView = inflater.inflate(R.layout.transaction_list_header,  parent, false);
		TextView dateHeader = (TextView)convertView.findViewById(R.id.transaction_header_text);
		DateTime dateTime = _Transactions[position].date;
		dateHeader.setText(dateTime.dateToString(DateTime.MMDDCCYY));
		convertView.setTag(dateHeader);
		return convertView;
	}

	@Override
	public long getHeaderId(int position) {
		return _Transactions[position].date.getTimeInMilliseconds();
//		return 1;
	}

	
}
