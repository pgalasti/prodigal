package com.gwiz.prodigalbudget2.data;

import android.content.ContentValues;

import com.gwiz.androidutils.db.AQueryRecord;
import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

public class BudgetRecord extends AQueryRecord {

	public String 	budgetGuid;
	public String	budgetName;
	public float	limit;
	public float	current;
	
	
	public final float getCurrentToLimitRatio() {
		
		float ratio = 0.0f;
		if(limit > 0.0)
			ratio = current/limit;
		
		return ratio;
	}
	
	
	@Override
	public int update(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("budget_name", budgetName);
		cv.put("lim", limit);
		cv.put("current", current);
		
		return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "budget_guid=?", new String[]{budgetGuid});
	}

	@Override
	public long insert(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("budget_guid", budgetGuid);
		cv.put("budget_name", budgetName);
		cv.put("lim", limit);
		cv.put("current", current);
		
		long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
		if( insertID == -1 )
			throw new DBException("Failure inserting record.", this);
		
		return insertID;
	}

	@Override
	public int delete(Database db) throws DBException {
		return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "budget_guid=?", new String[]{budgetGuid});
	}

	@Override
	public void select(Database db, String sqlQuery) throws DBException {
		m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
		this.getFirst();
		
	}

	@Override
	public void select(Database db, int keys) throws DBException {
		if( keys < 1 )
		{
			m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
			this.getFirst();
			return;
		}
		
		StringBuilder sbSelection = new StringBuilder("budget_guid=?");
		String[] selecetionArgs = new String[keys];
		selecetionArgs[0] = budgetGuid;
		if( keys > 1 )
		{
			sbSelection.append(" AND budget_name=?");
			selecetionArgs[1] = budgetName;
		}
		if( keys > 2 )
		{
			sbSelection.append(" AND lim=?");
			selecetionArgs[2] = String.valueOf(limit);
		}
		if( keys > 3 )
		{
			sbSelection.append(" AND current=?");
			selecetionArgs[3] = String.valueOf(current);
		}
		
		m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
		this.getFirst();
		
	}

	@Override
	protected void BindData() {
		short index = 0;
		budgetGuid = m_Cursor.getString(index++);
		budgetName = m_Cursor.getString(index++);
		limit = m_Cursor.getFloat(index++);
		current = m_Cursor.getFloat(index++);
	}

	protected static IRecordAttributes attributes;
	static 	{
		attributes =  new IRecordAttributes() {

			@Override
			public String getTableName() {
				return "budget";
			}

			@Override
			public int getTableColumns() {
				return 4;
			}

			@Override
			public String getSQLCreateStatement() {
				StringBuilder SB = new StringBuilder();
				SB.append("CREATE TABLE ").append(getTableName()).append("(")
				.append("budget_guid TEXT PRIMARY KEY, ")
				.append("budget_name TEXT, ")
				.append("lim FLOAT, ")
				.append("current FLOAT); ");

				return SB.toString();
			}

			@Override
			public String getSQLAlterSchema(int oldVersion, int newVersion) {
				return "";
			}
		};
	}
	
	@Override
	public IRecordAttributes getRecordAttributes() {
		
		return attributes;
	}

	@Override
	public String getDebugDetail() {
		StringBuilder SB = new StringBuilder();
		SB.append("Table:").append(attributes.getTableName()).append("\n")
		.append("Columns:").append(attributes.getTableColumns()).append("\n")
		.append("budget_guid:").append("TEXT:").append(budgetGuid).append("\n")
		.append("budget_name:").append("TEXT:").append(budgetName).append("\n")
		.append("lim:").append("FLOAT:").append(limit).append("\n")
		.append("current:").append("FLOAT:").append(current).append("\n");
		return SB.toString();
	}

	@Override
	public BudgetRecord clone()
	{
		BudgetRecord newRecord = new BudgetRecord();
		
		newRecord.budgetGuid = this.budgetGuid;
		newRecord.budgetName = this.budgetName;
		newRecord.current = this.current;
		newRecord.limit = this.limit;
		
		return newRecord;
	}
}
