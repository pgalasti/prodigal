package com.gwiz.prodigalbudget2.data;

import android.content.ContentValues;

import com.gwiz.androidutils.db.AQueryRecord;
import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;

public class NotificationRecord extends AQueryRecord{

	public String	noteificationGuid;
	public boolean 	bReported;
	public String	notificationMessage;
	
	
	@Override
	public int update(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("reported", bReported ? 1 : 0);
		cv.put("message", notificationMessage);
		
		return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "note_guid=?", new String[]{noteificationGuid});
	}

	@Override
	public long insert(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("note_guid", noteificationGuid);
		cv.put("reported", bReported ? 1 : 0);
		cv.put("message", notificationMessage);
		
		
		long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
		if( insertID == -1 )
			throw new DBException("Error inserting record.", this);
		
		return insertID;
	}

	@Override
	public int delete(Database db) throws DBException {
		return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "note_guid=?", new String[]{noteificationGuid});
	}

	@Override
	public void select(Database db, String sqlQuery) throws DBException {
		m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
		this.getFirst();
	}

	@Override
	public void select(Database db, int keys) throws DBException {
		if( keys < 1 )
		{
			m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
			this.getFirst();
			return;
		}
		
		StringBuilder sbSelection = new StringBuilder("note_guid=?");
		String[] selecetionArgs = new String[keys];
		selecetionArgs[0] = noteificationGuid;
		if( keys > 1 )
		{
			sbSelection.append(" AND reported=?");
			selecetionArgs[1] = bReported ? String.valueOf(1) : String.valueOf(0);
		}
		if( keys > 2 )
		{
			sbSelection.append(" AND message=?");
			selecetionArgs[2] = notificationMessage;
		}
		
		
		m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
		this.getFirst();
	}

	@Override
	protected void BindData() {
		short index = 0;
		noteificationGuid = m_Cursor.getString(index++);
		bReported = m_Cursor.getInt(index++) == 1 ? true : false;
		notificationMessage = m_Cursor.getString(index++);
	}

	protected static IRecordAttributes attributes;
	static 	{
		attributes =  new IRecordAttributes() {

			@Override
			public String getTableName() {
				return "notif";
			}

			@Override
			public int getTableColumns() {
				return 3;
			}

			@Override
			public String getSQLCreateStatement() {
				StringBuilder SB = new StringBuilder();
				SB.append("CREATE TABLE ").append(getTableName()).append("(")
				.append("note_guid TEXT PRIMARY KEY, ")
				.append("reported INTEGER, ")
				.append("message TEXT);");

				return SB.toString();
			}

			@Override
			public String getSQLAlterSchema(int oldVersion, int newVersion) {
				return "";
			}
		};
	}
	
	@Override
	public IRecordAttributes getRecordAttributes() {
		return attributes;
	}

	@Override
	public String getDebugDetail() {
		StringBuilder SB = new StringBuilder();
		SB.append("Table:").append(attributes.getTableName()).append("\n")
		.append("Columns:").append(attributes.getTableColumns()).append("\n")
		.append("note_guid:").append("TEXT:").append(noteificationGuid).append("\n")
		.append("reported:").append("INTEGER:").append(bReported).append("\n")
		.append("message:").append("TEXT:").append(notificationMessage).append("\n");
		return SB.toString();
	}

}
