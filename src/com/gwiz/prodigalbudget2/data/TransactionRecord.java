package com.gwiz.prodigalbudget2.data;

import android.content.ContentValues;

import com.gwiz.androidutils.db.AQueryRecord;
import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;
import com.gwiz.androidutils.util.DateTime;

public class TransactionRecord extends AQueryRecord {

	public String 	transGuid;
	public String 	budgetGuid;
	public char		type;
	public float	amount;
	public String	comment;
	public DateTime	date;
	public String	picID;
	
	@Override
	public int update(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("budget_guid", budgetGuid);
		cv.put("type", Character.toString(type));
		cv.put("amount", amount);
		cv.put("comment", comment);
		cv.put("dt", date.getTimeInMilliseconds());
		cv.put("pic_id", picID);
		
		return db.getWritableDatabase().update(getRecordAttributes().getTableName(), cv, "trans_guid=?", new String[]{transGuid});
	}

	@Override
	public long insert(Database db) throws DBException {
		ContentValues cv = new ContentValues();
		cv.put("trans_guid", transGuid);
		cv.put("budget_guid", budgetGuid);
		cv.put("type", Character.toString(type));
		cv.put("amount", amount);
		cv.put("comment", comment);
		cv.put("dt", date.getTimeInMilliseconds());
		cv.put("pic_id", picID);
		
		long insertID = db.getWritableDatabase().insert(getRecordAttributes().getTableName(), null, cv);
		if( insertID == -1 )
			throw new DBException("Failure inserting record.", this);
		
		return insertID;
	}

	@Override
	public int delete(Database db) throws DBException {
		return db.getWritableDatabase().delete(getRecordAttributes().getTableName(), "trans_guid=?", new String[]{transGuid});
	}

	@Override
	public void select(Database db, String sqlQuery) throws DBException {
		m_Cursor = db.getReadableDatabase().rawQuery(sqlQuery, null);
		this.getFirst();
	} 

	@Override
	public void select(Database db, int keys) throws DBException {
		if( keys < 1 )
		{
			m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, null, null, null, null, null);
			this.getFirst();
			return; 
		} 
		
		StringBuilder sbSelection = new StringBuilder("trans_guid=?");
		String[] selecetionArgs = new String[keys];
		selecetionArgs[0] = transGuid;
		if( keys > 1 )
		{
			sbSelection.append(" AND budget_guid=?");
			selecetionArgs[1] = budgetGuid;
		}
		if( keys > 2 )
		{
			sbSelection.append(" AND type=?");
			selecetionArgs[2] = String.valueOf(type);
		}
		if( keys > 3 )
		{
			sbSelection.append(" AND amount=?");
			selecetionArgs[3] = String.valueOf(amount);
		}
		if( keys > 4 )
		{
			sbSelection.append(" AND comment=?");
			selecetionArgs[4] = comment;
		}
		if( keys > 5 )
		{
			sbSelection.append(" AND dt=?");
			selecetionArgs[5] = String.valueOf(date);
		}
		if( keys > 6 )
		{
			sbSelection.append(" AND pic_id=?");
			selecetionArgs[6] = picID;
		}
		
		m_Cursor = db.getReadableDatabase().query(getRecordAttributes().getTableName(), null, sbSelection.toString(), selecetionArgs, null, null, null);
		this.getFirst();
	}

	@Override
	protected void BindData() {
		short index = 0;
		transGuid = m_Cursor.getString(index++);
		budgetGuid = m_Cursor.getString(index++);
		type = m_Cursor.getString(index++).charAt(0);
		amount = m_Cursor.getFloat(index++);
		comment = m_Cursor.getString(index++);
		date.setDateTime(m_Cursor.getLong(index++));
		picID = m_Cursor.getString(index++);
	}

	protected static IRecordAttributes attributes;
	static {
		attributes = new IRecordAttributes() {

			@Override
			public String getTableName() {
				return "trans";
			}

			@Override
			public int getTableColumns() {
				return 7;
			}

			@Override
			public String getSQLCreateStatement() {
				StringBuilder SB = new StringBuilder();
				SB.append("CREATE TABLE ").append(getTableName()).append("(")
				.append("trans_guid TEXT PRIMARY KEY, ")
				.append("budget_guid TEXT, ")
				.append("type CHARACTER(1), ")
				.append("amount FLOAT, ")
				.append("comment TEXT, ")
				.append("dt LONG, ")
				.append("pic_id TEXT);");

				return SB.toString();
			}

			@Override
			public String getSQLAlterSchema(int oldVersion, int newVersion) {
				return "";
			}
		};
	}

	@Override
	public IRecordAttributes getRecordAttributes() {
		return attributes;
	}

	@Override
	public String getDebugDetail() {
		StringBuilder SB = new StringBuilder();
		SB.append("Table:").append(attributes.getTableName()).append("\n")
		.append("Columns:").append(attributes.getTableColumns()).append("\n")
		.append("trans_guid:").append("TEXT:").append(transGuid).append("\n")
		.append("type:").append("CHARACTER:").append(type).append("\n")
		.append("amount:").append("FLOAT:").append(amount).append("\n")
		.append("comment:").append("TEXT:").append(comment).append("\n")
		.append("dt:").append("LONG:").append(date).append("\n")
		.append("pic_id:").append("TEXT:").append(picID).append("\n");
		return SB.toString();
	}

}
