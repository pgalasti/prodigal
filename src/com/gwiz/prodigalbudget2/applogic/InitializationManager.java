package com.gwiz.prodigalbudget2.applogic;

import java.io.IOException;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class InitializationManager {

	private SharedPreferences preferences;

	private static final String FIRST_RUN_KEY = "first_time_run";
	private static final String FIRST_RUN_BUDGET_KEY = "first_time_run_budget_list";
	private static final String ACTIVITY = "activity";
	
	public enum LastActivityEnum {
		BudgetActivity,
		StatisticsActivity,
		AddBudgetActivity,
		LaunchActivity,
		TransactionsActivity,
	}
	
	public InitializationManager(Context context) {
		preferences = PreferenceManager.getDefaultSharedPreferences(context);
	}
	
	public boolean checkForFirstTimeRun() {
		boolean result = preferences.getBoolean(FIRST_RUN_KEY, true);
		return result;
	}
	
	public void setFirstTimeRun() throws IOException {
		if( !preferences.edit().putBoolean(FIRST_RUN_KEY, false).commit() )
			throw new IOException("Unable to commit changes that the app ran for the first time.");
	}
	
	public boolean checkForFirstTimeBudgetRun() {
		boolean result = preferences.getBoolean(FIRST_RUN_BUDGET_KEY, true);
		return result;
	}
	
	public void setFirstTimeBudgetRun() throws IOException {
		if( !preferences.edit().putBoolean(FIRST_RUN_BUDGET_KEY, false).commit() )
			throw new IOException("Unable to commit changes that the app ran for the first time budget.");
	}
	
	public LastActivityEnum getLastActivity() {
		int activty = preferences.getInt(ACTIVITY, 0);
		if( activty == 0 )
			return LastActivityEnum.BudgetActivity;
		
		return LastActivityEnum.StatisticsActivity;
	}
	
	public void setLastActivity(LastActivityEnum lastActivity) {
		int value;
		if( lastActivity == LastActivityEnum.BudgetActivity )
			value = 0;
		else
			value = 1;
		
		preferences.edit().putInt(ACTIVITY, value).commit();
	}
}
