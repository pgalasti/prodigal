package com.gwiz.prodigalbudget2.applogic;

import java.util.ArrayList;
import java.util.Iterator;

import com.gwiz.androidutils.db.Database;
import com.gwiz.prodigalbudget2.data.BudgetRecord;
import com.gwiz.prodigalbudget2.data.TransactionRecord;
import com.gwiz.prodigalbudget2.database.DBRecordException;

public class BudgetList {

	private String m_BudgetGUID;
	private ArrayList<TransactionRecord> m_Transactions;
	
	public BudgetList(Database db, BudgetRecord budgetRecord) throws DBRecordException.RecordNotFoundException
	{
		m_BudgetGUID = budgetRecord.budgetGuid; 
		budgetRecord.select(db, 1);
		if( !budgetRecord.getGoodState() )
			throw new DBRecordException.RecordNotFoundException("Unable to find specific budget record in database for list.");

		TransactionRecord currentTransaction = new TransactionRecord();
		m_Transactions = new ArrayList<TransactionRecord>();
		// Populate transaction list with 'currentTransaction' 
	}
	
	public float SumTotal()
	{
		float sum = 0.0f;
		for(Iterator<TransactionRecord> iter = m_Transactions.iterator(); iter.hasNext();)
			sum += iter.next().amount;
				
		return sum;
	}
	
	
}
