package com.gwiz.prodigalbudget2.applogic;

import android.content.Context;

public class InitializationSingleton {

	private volatile static InitializationManager manager;
	
	/** Construction not available outside singleton*/
	private InitializationSingleton() {}
	
	public static InitializationManager getInstance(Context context) {
		if( manager == null )
			manager = new InitializationManager(context);
		return manager;
	}
}
