package com.gwiz.prodigalbudget2.activity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.gwiz.androidutils.db.IQueryWritable;
import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.applogic.InitializationManager.LastActivityEnum;
import com.gwiz.prodigalbudget2.data.BudgetRecord;
import com.gwiz.prodigalbudget2.data.NotificationRecord;
import com.gwiz.prodigalbudget2.database.DBRecordException;
import com.gwiz.prodigalbudget2.database.ProdigalBudgetDatabase;
import com.gwiz.prodigalbudget2.database.ProdigalDatabaseSingleton;

public class LaunchActivity extends ProdigalActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		if( initManager.checkForFirstTimeRun() )
		{
			ProdigalBudgetDatabase db = ProdigalDatabaseSingleton.getInstance(getApplicationContext());
			ArrayList<IQueryWritable> recordList = getInitialDatabaseRecords();
			
			try
			{
				for( Iterator<IQueryWritable> recordIter = recordList.iterator(); recordIter.hasNext(); )
				{
					IQueryWritable nextRecord = recordIter.next();
					nextRecord.insert(db);
				}
			}
			catch(DBRecordException.DatabaseWritableException e)
			{
				//TODO handle error for failing to put tutorial records.
			}
			
			try {
				initManager.setFirstTimeRun();
			} catch (IOException e) {
				PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().clear();
				Toast.makeText(getBaseContext(), "The app was unable to apply first time run optimizations!", Toast.LENGTH_LONG).show();
			}
		}
		
		// TODO
		if( initManager.getLastActivity() == LastActivityEnum.BudgetActivity 
				|| initManager.getLastActivity() == LastActivityEnum.StatisticsActivity ) // For now
		{
			Intent newIntent = new Intent(getApplicationContext(), com.gwiz.prodigalbudget2.activity.MainScreenActivity.class); 
			startActivity(newIntent);
			finish();
		}
	}
	
	private ArrayList<IQueryWritable> getInitialDatabaseRecords()
	{
		ArrayList<IQueryWritable> databaseRecords = new ArrayList<IQueryWritable>();
		
		BudgetRecord budgetRecord;
		
		budgetRecord = new BudgetRecord();
		budgetRecord.budgetGuid = UUID.randomUUID().toString();
		budgetRecord.budgetName = getBaseContext().getString(R.string.en_sample_mortgage);
		budgetRecord.limit = 1000.0f;
		databaseRecords.add(budgetRecord);
		
		budgetRecord = new BudgetRecord();
		budgetRecord.budgetGuid = UUID.randomUUID().toString();
		budgetRecord.budgetName = getBaseContext().getString(R.string.en_sample_grocery);
		budgetRecord.limit = 150.0f;
		databaseRecords.add(budgetRecord);
		
		budgetRecord = new BudgetRecord();
		budgetRecord.budgetGuid = UUID.randomUUID().toString();
		budgetRecord.budgetName = getBaseContext().getString(R.string.en_sample_eatout);
		budgetRecord.limit = 50.0f;
		databaseRecords.add(budgetRecord);
		
		budgetRecord = new BudgetRecord();
		budgetRecord.budgetGuid = UUID.randomUUID().toString();
		budgetRecord.budgetName = getBaseContext().getString(R.string.en_sample_gas);
		budgetRecord.limit = 120.0f;
		databaseRecords.add(budgetRecord);
		
		// Add rest...
		
		NotificationRecord notificationRecord;
		notificationRecord = new NotificationRecord();
		notificationRecord.noteificationGuid = UUID.randomUUID().toString();
		notificationRecord.bReported = false;
		notificationRecord.notificationMessage = getBaseContext().getString(R.string.str_welcome);
		databaseRecords.add(notificationRecord);
		
		notificationRecord = new NotificationRecord();
		notificationRecord.noteificationGuid = UUID.randomUUID().toString();
		notificationRecord.bReported = false;
		notificationRecord.notificationMessage = getBaseContext().getString(R.string.str_notification_tutorial);
		databaseRecords.add(notificationRecord);
		
		// Add rest...
		return databaseRecords;
	}

	@Override
	public LastActivityEnum activityIdentity() {
		return LastActivityEnum.LaunchActivity;
	}

}
