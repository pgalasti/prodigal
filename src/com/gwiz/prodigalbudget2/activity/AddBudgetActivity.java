package com.gwiz.prodigalbudget2.activity;

import java.util.UUID;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gwiz.androidutils.logging.Logger;
import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.applogic.InitializationManager.LastActivityEnum;
import com.gwiz.prodigalbudget2.data.BudgetRecord;
import com.gwiz.prodigalbudget2.database.DBRecordException;
import com.gwiz.prodigalbudget2.database.ProdigalBudgetDatabase;
import com.gwiz.prodigalbudget2.database.ProdigalDatabaseSingleton;

public class AddBudgetActivity extends ProdigalActivity {

	public final static String ActivityTypeKey = "launch_type";
	public final static String ActivityMessageAddBudget = "add_budget";
	public final static String ActivityMessageEditBudget = "edit_budget";
	
	Button addEditButton;
	EditText budgetNameEditText;
	EditText budgetLimitEditText;

	boolean isAddBudget = false;
	boolean isEditBudget = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_budget_layout);
		
		budgetNameEditText = (EditText)findViewById(R.id.add_budget_name_edittext);
		budgetLimitEditText = (EditText)findViewById(R.id.add_budget_limit_edittext);
		addEditButton = (Button)findViewById(R.id.add_budget_save_button);
		
		// Debug code
		Logger log = new Logger(getBaseContext(), "test.txt");
		log.writeToFile("This is a test");
		log.writeToFile("This is another test");
		log.writeToFile("This is a 3rd test");
		
		addEditButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if( isAddBudget )
				{
					try {
						AddBudget();
					} 
					catch (DBRecordException.DatabaseWritableException ex)
					{
						// TODO
					}
					catch (BadInputException ex)
					{
						Toast.makeText(getApplicationContext(), ex.getMessage() , Toast.LENGTH_LONG).show();
						return;
					}
				}
			}
		});
		Bundle activityExtras = getIntent().getExtras();
		String activityType = activityExtras.getString(ActivityTypeKey);
		isAddBudget = (activityType.compareToIgnoreCase(ActivityMessageAddBudget) == 0);
		isEditBudget = (activityType.compareToIgnoreCase(ActivityMessageEditBudget) == 0);
		
		
//		if( !isAddBudget && !isEditBudget )
//			throw new AndroidException("Launching budget add/edit activity was never sent a type.");
	}
	
	private void AddBudget() throws DBRecordException.DatabaseWritableException, BadInputException {
		if( budgetNameEditText.getText().toString().isEmpty() || budgetLimitEditText.getText().toString().isEmpty()	)
			return;
		
		BudgetRecord newBudgetRecord = new BudgetRecord();
		newBudgetRecord.budgetGuid = UUID.randomUUID().toString();
		newBudgetRecord.budgetName = budgetNameEditText.getText().toString();
		newBudgetRecord.limit = Float.parseFloat(budgetLimitEditText.getText().toString());
		
		if( newBudgetRecord.limit <= 0.0f )
			throw new BadInputException("A valid budget limit is required!");
		
		ProdigalBudgetDatabase db = ProdigalDatabaseSingleton.getInstance(getBaseContext());
		newBudgetRecord.insert(db);
		
		Toast.makeText(getApplicationContext(), newBudgetRecord.budgetName + " added." , Toast.LENGTH_LONG).show();
		finish();		
	}
	
	public class BadInputException extends Exception
	{
		private static final long serialVersionUID = -1580125185259922746L;
		
		public BadInputException(String message) {
			super(message);
		}
		
	}

	@Override
	public LastActivityEnum activityIdentity() {
		return LastActivityEnum.AddBudgetActivity;
	}
}
