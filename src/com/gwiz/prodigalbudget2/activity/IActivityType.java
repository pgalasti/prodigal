package com.gwiz.prodigalbudget2.activity;

import com.gwiz.prodigalbudget2.applogic.InitializationManager.LastActivityEnum;

public interface IActivityType
{
	public LastActivityEnum activityIdentity();
}
