package com.gwiz.prodigalbudget2.activity;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.View;
import android.view.View.OnClickListener;

import com.gwiz.androidutils.ui.enhancements.BlockablePager;
import com.gwiz.androidutils.ui.viewpageindicator.CirclePageIndicator;
import com.gwiz.androidutils.ui.viewpageindicator.PageIndicator;
import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.ui.QuestionView;

/*
 * How often do you receive your main source of income?
 * a) Monthly
 * b) Bi-weekly
 * c) Weekly
 * d) Specified Dates
 * e) I'll input it whenever
 * 
 * Would you like monthly surpluses to run into the next month?
 * a) Yes
 * b) No
 * 
 * Would you like reminders of bills or income?
 * a) Yes
 * b) No
 * 
 * Would you like a tutorial?
 * a) Yes
 * b) No
 * 
 */

public class QuestionnaireActivity extends FragmentActivity {

	QuestionPagerAdapter _sectionsPagerAdapter;
	PageIndicator _indicator;
	BlockablePager _viewPager;
	QuestionView[] questionArray = new QuestionView[4];
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_questions);

		_sectionsPagerAdapter = new QuestionPagerAdapter(
				getSupportFragmentManager());

		_viewPager = (BlockablePager) findViewById(R.id.question_pager);
		_viewPager.setAdapter(_sectionsPagerAdapter);
		
		_indicator = (CirclePageIndicator)findViewById(R.id.indicator);
		_indicator.setViewPager(_viewPager);
		
		questionArray[0] = new QuestionView(getBaseContext());
		questionArray[1] = new QuestionView(getBaseContext());
		questionArray[2] = new QuestionView(getBaseContext());
		questionArray[3] = new QuestionView(getBaseContext());
		
		questionArray[0].setQuestionText("How often do you receive your main source of income?");
		questionArray[1].setQuestionText("Would you like monthly surpluses to run into the next month?");
		questionArray[2].setQuestionText("Would you like reminders of bills or income?");
		questionArray[3].setQuestionText("Would you like a tutorial?");
		
		ArrayList<String> questionSelections = new ArrayList<String>();
		
		questionSelections.add("Monthly");
		questionSelections.add("Bi-Weekly");
		questionSelections.add("Weekly");
		questionSelections.add("Specified Dates");
		questionSelections.add("I'll input it when I get it");
		questionArray[0].setSelectionList((ArrayList<String>)questionSelections.clone());
		questionSelections.clear();
		
		questionSelections.add("Yes");
		questionSelections.add("No");
		questionArray[1].setSelectionList((ArrayList<String>)questionSelections.clone());
		questionSelections.clear();
		
		questionSelections.add("Yes");
		questionSelections.add("No");
		questionArray[2].setSelectionList((ArrayList<String>)questionSelections.clone());
		questionSelections.clear();
		
		questionSelections.add("Yes");
		questionSelections.add("No");
		questionArray[3].setSelectionList((ArrayList<String>)questionSelections.clone());
		questionSelections.clear();
		
		questionArray[0].addRadioButton("Blah");
		questionArray[1].addRadioButton("Blah");
		questionArray[2].addRadioButton("Blah");
		questionArray[3].addRadioButton("Blah");
	}
	
	public class QuestionPagerAdapter extends FragmentPagerAdapter {

		public QuestionPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;
			
			boolean exit = false;
			if(position == 3)
				exit = true;
			
			fragment = QuestionFragment.newInstance(questionArray[position].getText(), questionArray[position].getSelectionList(), exit);
			return fragment;
		}

		@Override
		public int getCount() {
			return 4;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "Test";
		}
	}
}
