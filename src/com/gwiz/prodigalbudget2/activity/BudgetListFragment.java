package com.gwiz.prodigalbudget2.activity;

import java.text.NumberFormat;
import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.data.BudgetRecord;
import com.gwiz.prodigalbudget2.database.ProdigalBudgetDatabase;
import com.gwiz.prodigalbudget2.database.ProdigalDatabaseSingleton;
import com.gwiz.prodigalbudget2.ui.BudgetItem;

public class BudgetListFragment extends Fragment {

	LinearLayout rootLayout;
	ArrayList<BudgetItem> budgetItems = new ArrayList<BudgetItem>();
	
	ProgressBar totalProgress;
	TextView totalAmount;
	TextView availableText;
	
	private Menu actionBarMenu;

	public BudgetListFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

	}

	@Override
	public void onStop() {
		super.onStop();
		budgetItems.clear();
		rootLayout.removeAllViews();
		
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_budgetlist, container, false);
		rootLayout = (LinearLayout) view.findViewById(R.id.budgetlist_main);

		totalProgress = (ProgressBar)view.findViewById(R.id.budget_total_progress);
		totalAmount = (TextView)view.findViewById(R.id.budget_amount_of);
		availableText = (TextView)view.findViewById(R.id.budget_income_available);
		
		availableText.setText("$1500.00 available of $3000.00 ($200.00 not in budget)");
		return view;	
	}

	@Override 
	public void onResume()
	{
		super.onResume();

		ProdigalBudgetDatabase db = ProdigalDatabaseSingleton.getInstance(rootLayout.getContext()); 
		BudgetRecord bufferBudgetRecord = new BudgetRecord();

		bufferBudgetRecord.select(db, 0);
		if( bufferBudgetRecord.getGoodState() )
		{
			float totalSpent = 0.0f;
			float total = 0.0f;
			
			int delay = 0;
			do
			{
				// Get totals
				totalSpent += bufferBudgetRecord.current;
				total += bufferBudgetRecord.limit;
				
				// Get animation going
				BudgetItem nextBudgetItem = new BudgetItem(rootLayout.getContext(), bufferBudgetRecord.clone());
				rootLayout.addView(nextBudgetItem);
				nextBudgetItem.startBudgetItemSlideAnimation(delay);
				
				nextBudgetItem.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						BudgetItem itemSelected = (BudgetItem)v;
						final BudgetRecord record = itemSelected.getBudgetRecord();
						
						String key = TransactionActivity.BudgetGUIDKey;
						String message = record.budgetGuid;

						Intent transactionListIntent = new Intent(getActivity().getApplicationContext(), TransactionActivity.class); 
						transactionListIntent.putExtra(key, message);
						startActivity(transactionListIntent);
					}
				});
				
				delay += 100;
			}while(bufferBudgetRecord.readNext());
			
			NumberFormat formatter = NumberFormat.getCurrencyInstance(); // move this to top
			StringBuilder sb = new StringBuilder(formatter.format(totalSpent));
			sb.append(" of ");
			sb.append(formatter.format(total));
			
			totalAmount.setText(sb.toString());
			totalProgress.setProgress(80);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.activity_budgetlist_menu, menu);
		actionBarMenu = menu;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch(item.getItemId())
		{
		case R.id.add_budget_btn:
			String key = AddBudgetActivity.ActivityTypeKey;
			String message = AddBudgetActivity.ActivityMessageAddBudget;

			Intent addBudgetIntent = new Intent(getActivity().getApplicationContext(), AddBudgetActivity.class);
			addBudgetIntent.putExtra(key, message );
			startActivity(addBudgetIntent);
			break;
		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

}
