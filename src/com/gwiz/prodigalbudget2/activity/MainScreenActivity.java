package com.gwiz.prodigalbudget2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.applogic.InitializationManager.LastActivityEnum;

public class MainScreenActivity extends ProdigalActivity {

	SectionsPagerAdapter mSectionsPagerAdapter;

	ViewPager mViewPager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_screen);

		mSectionsPagerAdapter = new SectionsPagerAdapter(
				getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setCurrentItem(1);
		
		Spinner titleSpinner = new Spinner(this);
		getActionBar().setCustomView(titleSpinner);
		getActionBar().setDisplayShowCustomEnabled(true);

		ArrayAdapter<String> barAdapter = new ArrayAdapter<String>(this, R.layout.action_spinner, 
			    R.id.ts_subtitle, getResources().getStringArray(R.array.subtitle_array));
		barAdapter.setDropDownViewResource(R.layout.action_spinner_row);
		titleSpinner.setAdapter(barAdapter);

		
		// temp code
//		Intent questionIntent = new Intent(getBaseContext().getApplicationContext(), QuestionnaireActivity.class); 
//		startActivity(questionIntent);
	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;;
			
			if( position == 0 ) {
				fragment = new BudgetListFragment();
			}
			else if( position == 1) {
				fragment = new NotificationFragment();	
			}
			else// if( position == 2) {
				fragment = new CalendarFragment();
//			}
			
			return fragment;
		}

		@Override
		public int getCount() {
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				return getString(R.string.budget_list_section);
			case 1:
				return getString(R.string.notification_section);
			case 2:
				return getString(R.string.cal_overview_section);
			}
			return null;
		}
	}

	@Override
	public LastActivityEnum activityIdentity() {
		return LastActivityEnum.BudgetActivity;
	}

}
