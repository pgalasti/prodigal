package com.gwiz.prodigalbudget2.activity;

import java.util.Arrays;
import java.util.Comparator;

import android.app.ActionBar;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;

import com.gwiz.androidutils.db.DBException;
import com.gwiz.androidutils.ui.stickylistheaders.StickyListHeadersListView;
import com.gwiz.androidutils.util.DateTime;
import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.applogic.InitializationManager.LastActivityEnum;
import com.gwiz.prodigalbudget2.data.BudgetRecord;
import com.gwiz.prodigalbudget2.data.TransactionRecord;
import com.gwiz.prodigalbudget2.database.ProdigalDatabaseSingleton;
import com.gwiz.prodigalbudget2.ui.StickyListTransactionAdapter;

public class TransactionActivity extends ProdigalActivity {

	public final static String BudgetGUIDKey = "budget_guid";
	private BudgetRecord budgetRecord;
	
	private TransactionRecord[] TestRecords = new TransactionRecord[25];
	DateTime beginDate;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transactions_list);
		
		beginDate = new DateTime();
		beginDate.changeDays(-90);
		
		BudgetRecord budgetRecord = new BudgetRecord();
		
		
		ActionBar test = getActionBar(); 
		test.show();
		test.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#009ACD")));
		LoadTestRecords(); 
		Bundle activityExtras = getIntent().getExtras();
		budgetRecord.budgetGuid = activityExtras.getString(BudgetGUIDKey, "");
		try {
			budgetRecord.select(ProdigalDatabaseSingleton.getInstance(getBaseContext()), 1);
		} catch( DBException dbException )
		{
			// Handle
		}
		
		setTitle(budgetRecord.budgetName);
		
		StickyListHeadersListView stickyList = (StickyListHeadersListView)findViewById(R.id.transaction_list);
		StickyListTransactionAdapter adapter = new StickyListTransactionAdapter(getBaseContext(), TestRecords); 
		stickyList.setAdapter(adapter);
		
	}
	@Override
	public LastActivityEnum activityIdentity() {
		return LastActivityEnum.TransactionsActivity;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	private void LoadTestRecords()
	{
		DateTime testDate = new DateTime(1, 1, 2013);
		for(int i = 0; i < 25; i++)
		{
			TestRecords[i] = new TransactionRecord();
			TestRecords[i].amount = 50.0f;
			TestRecords[i].comment = "Number " + i;
			
			if( i % 3 == 0)
				testDate.changeDays(1);
			TestRecords[i].date = new DateTime(testDate.getTimeInMilliseconds());
			TestRecords[i].type = 'W';
		}
		
		TransactionComparator byDate = new TransactionComparator();
		Arrays.sort(TestRecords, byDate);
	}
	
	class TransactionComparator implements Comparator<TransactionRecord>
	{

		@Override
		public int compare(TransactionRecord lhs, TransactionRecord rhs) {
			int debug = DateTime.differenceBetweenDates(rhs.date, lhs.date);
			return debug*-1;
		}
		
	}
}
