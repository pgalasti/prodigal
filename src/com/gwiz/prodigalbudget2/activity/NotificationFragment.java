package com.gwiz.prodigalbudget2.activity;

import android.os.Bundle; 
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gwiz.prodigalbudget2.R;
import com.gwiz.prodigalbudget2.data.NotificationRecord;
import com.gwiz.prodigalbudget2.database.ProdigalBudgetDatabase;
import com.gwiz.prodigalbudget2.database.ProdigalDatabaseSingleton;
import com.gwiz.prodigalbudget2.ui.MessageBox;
import com.gwiz.prodigalbudget2.ui.MessageBoxManager;


public class NotificationFragment extends Fragment {

	private ViewGroup rootDisplay;
	private MessageBoxManager messageManager;
	private Menu actionBarMenu;
	
	public NotificationFragment() {}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.activity_main, container, false);
		rootDisplay = (LinearLayout) view.findViewById(R.id.mainlayout);
		return view;	
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		
		if( messageManager.size() > 0 )
			inflater.inflate(R.menu.activity_main_screen, menu);
		else
			inflater.inflate(R.menu.main_menu_normal, menu);
		actionBarMenu = menu;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		switch(item.getItemId())
		{
		case R.id.main_menu_clear:
			messageManager.clearAll();
			actionBarMenu.removeItem(R.id.main_menu_clear);
			break;
		default:
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		NotificationRecord notificationView = new NotificationRecord();
		messageManager = new MessageBoxManager(rootDisplay);
		MessageBox box;
		ProdigalBudgetDatabase db = ProdigalDatabaseSingleton.getInstance(getActivity()); 
		StringBuilder sbQuery = new StringBuilder();
		sbQuery.append("SELECT * FROM ").append(notificationView.getRecordAttributes().getTableName())
		.append(" WHERE reported=0;");
		
		boolean hasAlerts = false;
		notificationView.select(db, sbQuery.toString());
		if( notificationView.getGoodState() )
		{
			hasAlerts = true;
			do
			{
				box = messageManager.addMessage(notificationView);
				box.getCancelImage().setOnClickListener(cancelClickListener);
				box.setOnClickListener(cancelClickListener);
			}while(notificationView.readNext());
		}
		
		if( !hasAlerts )
		{
			
		}
	}
	
	public OnClickListener cancelClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			if( v.getId() == R.id.info_cancel_notification )
			{
				MessageBox box = (MessageBox) v.getParent().getParent().getParent();
				messageManager.removeMessage(box);
				box.cancelNotifiaction();
				if( messageManager.size() < 1 )
				{
					messageManager.clearAll();
					actionBarMenu.removeItem(R.id.main_menu_clear);
				}
			}
		}
	};
}
