package com.gwiz.prodigalbudget2.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public final class QuestionFragment extends Fragment {

	private String _question;
	private ArrayList<String> _selection = new ArrayList<String>();
	private View view;
	private boolean _loaded = false;
	private boolean lastFragment;
	
	public static QuestionFragment newInstance(String question, ArrayList<String> selections, boolean exit) {
		QuestionFragment fragment = new QuestionFragment();
		fragment._question = question;
		fragment._selection = selections;
		fragment.lastFragment = exit;
		return fragment;
    }
	
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(com.gwiz.prodigalbudget2.R.layout.question_layout, container, false);
		TextView questionText = (TextView) view.findViewById(com.gwiz.prodigalbudget2.R.id.question_text);
		RadioGroup radioButtons = (RadioGroup) view.findViewById(com.gwiz.prodigalbudget2.R.id.question_radio_group);
		
		
		questionText.setText(_question);
		
		for(String selectionText : _selection)
		{
			RadioButton button = new RadioButton(container.getContext());
			button.setText(selectionText);
			radioButtons.addView(button);
		}
		
		if( lastFragment )
		{
			Button button = new Button(getActivity().getBaseContext());
			LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			button.setText("Finish");
			button.setGravity(Gravity.BOTTOM | Gravity.RIGHT);
			((LinearLayout)view).addView(button); // Dirty
			
			button.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent launchActivityIntent = new Intent(getActivity().getApplicationContext(), LaunchActivity.class); 
					startActivity(launchActivityIntent);
					getActivity().finish();
				}
			});
		}
		return view;
//		LinearLayout layout = new LinearLayout(getActivity());
//		layout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
//		layout.setGravity(Gravity.TOP);
//		layout.addView(_question);
//		
//		_loaded = true;
//		return layout;
		
		
	}
	
	@Override
	public void onPause() {
		super.onPause();
	}
	
	
	@Override
	public void onResume()
	{
		super.onResume();
	}
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if( savedInstanceState != null )
        {
        	int a = 0;
        	int b = a +1;
        }
	}
	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        outState.putInt("button_selection", 1);
    }
	 
};