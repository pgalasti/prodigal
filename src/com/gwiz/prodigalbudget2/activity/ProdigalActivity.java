package com.gwiz.prodigalbudget2.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.gwiz.prodigalbudget2.applogic.InitializationManager;
import com.gwiz.prodigalbudget2.applogic.InitializationSingleton;

public abstract class ProdigalActivity extends FragmentActivity implements IActivityType {
	
	protected InitializationManager initManager = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		initManager = InitializationSingleton.getInstance(getApplicationContext());
	}
	
	@Override
	protected void onStop()
	{
		super.onStop();
		initManager.setLastActivity(activityIdentity());
	}
}
