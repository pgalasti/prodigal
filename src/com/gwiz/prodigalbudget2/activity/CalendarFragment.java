package com.gwiz.prodigalbudget2.activity;

import android.os.Bundle; 
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.gwiz.prodigalbudget2.R;

public class CalendarFragment extends Fragment {

	private CalendarView rootCalendar;
	private Menu actionBarMenu;
	
	public CalendarFragment() {}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.activity_calendar_main, container, false);
		rootCalendar = (CalendarView) view.findViewById(R.id.calendar_main);
		return view;	
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.activity_calendar_menu, menu);
		actionBarMenu = menu;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}
}
