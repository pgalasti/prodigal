package com.gwiz.prodigalbudget2.database;

import android.content.Context;

import com.gwiz.androidutils.db.Database;
import com.gwiz.androidutils.db.IRecordAttributes;
import com.gwiz.prodigalbudget2.data.BudgetRecord;
import com.gwiz.prodigalbudget2.data.NotificationRecord;
import com.gwiz.prodigalbudget2.data.TransactionRecord;

public class ProdigalBudgetDatabase extends Database {

	private String _dbName;
	
	public ProdigalBudgetDatabase(Context context, String name, int version) {
		super(context, name, version);
		_dbName = name;
	}

	@Override
	protected void setViewList() {
		BudgetRecord budget = new BudgetRecord();
		NotificationRecord notif = new NotificationRecord();
		TransactionRecord trans = new TransactionRecord();
		
		viewList.add(budget.getRecordAttributes());
		viewList.add(notif.getRecordAttributes());
		viewList.add(trans.getRecordAttributes());
	}

	@Override
	public String getDatabaseName()	{
		return _dbName;
	}
	
	@Override
	public String getDBMSName() {
		return "SQLite3";
	}
	
	@Override
	public String getDBMSVersion()	{
		return "3"; // Not actually sure. Not important for this project though.
	}
}
