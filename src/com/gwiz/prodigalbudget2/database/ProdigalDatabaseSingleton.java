package com.gwiz.prodigalbudget2.database;

import android.content.Context;

public class ProdigalDatabaseSingleton {

	private static ProdigalBudgetDatabase db;
	private ProdigalDatabaseSingleton() {}
	
	public static ProdigalBudgetDatabase getInstance(Context context) {
		if( db == null ) {
			db = new ProdigalBudgetDatabase(context, "prodigal.db", 1);
		}
		
		return db;
	}
}
