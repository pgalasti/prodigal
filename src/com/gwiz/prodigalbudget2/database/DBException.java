package com.gwiz.prodigalbudget2.database;

import android.database.sqlite.SQLiteException;

public abstract class DBException extends SQLiteException {

	private static final long serialVersionUID = -8716221072840674483L;
	
	public static class UnspecifiedException extends SQLiteException {
		
		private static final long serialVersionUID = -682067261404070090L;

		public UnspecifiedException(String message)
		{
			super(message);
		}
		
	}

}
