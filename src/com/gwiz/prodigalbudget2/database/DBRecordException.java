package com.gwiz.prodigalbudget2.database;

import android.database.sqlite.SQLiteException;

public abstract class DBRecordException extends SQLiteException {

	private static final long serialVersionUID = 8036972226837963604L;

	public static class RecordNotFoundException extends SQLiteException
	{
		private static final long serialVersionUID = 1579629375704591069L;
		
		public RecordNotFoundException(String message) {
			super(message);
		}
	}
	
	public static class DatabaseWritableException extends SQLiteException {

		private static final long serialVersionUID = -2948279720533591572L;
		public enum ErrorType
		{
			Insert,
			Update,
			Delete,
		}
		 
		@SuppressWarnings("unused")
		private ErrorType type;
		public DatabaseWritableException(String message, ErrorType type) {
			super(message);
			this.type = type;
		}
	}
	
	public static class UnspecifiedException extends SQLiteException
	{
		private static final long serialVersionUID = 4658314616008636429L;

		public UnspecifiedException(String message) {
			super(message);
		}
	}
}
